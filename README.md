# SOR I - TP Threads

SOR I (Sistemas Operativos y Redes I).
Trabajo sobre threading realizado en C.

## Sobre los ejercicios

Se trabajó sobre threads y semáforos en C. El objetivo del TP era demostrar el conocimiento de threading y sincronización de procesos con semáforos.

- Shell.c es un simulador de terminal, que utiliza un proceso hijo para manejar los comandos que introduce el usuario.
- Se demostró la utilidad de threads para hacer paralelismo de datos y de tareas (sumaDigitosParalelismoDatos.c y  variasOperacionesParalelismoTareas.c).
- Tanto Consumidor.c como Jugar.c y JugarConPrioridad.c son ejercicios de sincronización de procesos.
