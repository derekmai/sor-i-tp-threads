#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>


double promedio;
int cantidadDigitos = 10000;
int numberArray[10000];
int aparicionDigitos[10] = {0,0,0,0,0,0,0,0,0,0};
int cantidadPrimos=0;
int sumaPrimos =0;
int indice=0;

void *sacarPromedio()
{
	int sumaParcial;
	sumaParcial=0;
	for (int i = 0; i<cantidadDigitos;i++)
	{
		sumaParcial+=numberArray[i];
	}

	promedio = (double)sumaParcial / (double)cantidadDigitos;
	printf("el promedio da %f\n",promedio);
	pthread_exit(0); 
}

void *apariciones()
{
	for (int i = 0; i<cantidadDigitos;i++)
	{
		int actual = numberArray[i];
		for (int j = 0; j < 10; j++)
		{
			if (actual == j)
			{
				aparicionDigitos[j]++;
			}
		}
	}
	int max = 0;
	for (int k = 0; k < 10;k++)
	{
		if (aparicionDigitos[k]>max)
		{
			max = aparicionDigitos[k];
			indice = k;
		}
		printf("el digito %i aparece %i veces\n",k,aparicionDigitos[k]);
	}
	printf("el digito con mas apariciones es %i\n",indice);
	pthread_exit(0); 
}

void *calculoPrimos()
{
	for (int i = 0; i<cantidadDigitos;i++)
	{
		int actual = numberArray[i];
		int cantidad=0;
		for (int j = 1; j<actual;j++)
		{
			if (actual%j==0)
			{
				cantidad++;
			}
		}
		if (cantidad==2)
		{
			cantidadPrimos++;
			sumaPrimos+=actual;
		}
	}
	printf("la cantidad de digitos primos es %i\n",cantidadPrimos);
  printf("la suma de los digitos primos es %i\n",sumaPrimos);
	pthread_exit(0); 
}

int main()
{

	FILE *myFile;
	myFile = fopen("10milDigitosDePi_separados.txt", "r");

	//leer archivo en un array

	int i;

	if (myFile == NULL){
	printf("Error en la lectura\n");
	exit (0);
	}

	for (i = 0; i < cantidadDigitos; i++){
	fscanf(myFile, "%d,", &numberArray[i] );
	}
	pthread_t p1,p2,p3;

	pthread_create(&p1,NULL,&sacarPromedio,NULL);
	pthread_create(&p2,NULL,apariciones,NULL);
	pthread_create(&p3,NULL,&calculoPrimos,NULL);

	pthread_join(p1,NULL);
	pthread_join(p2,NULL);
	pthread_join(p3,NULL);


  fclose(myFile);

  return 0;
}
