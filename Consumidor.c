#include <stdio.h>//para hacer printf
#include <stdlib.h>//para usar exit y funciones de libreria standard
#include <pthread.h>//para usar threads
#include <semaphore.h>//para usar semaforos
#define ITERACIONES 5
pthread_mutex_t mutex;
sem_t sem_escribir, sem_leer;
int datos = 0;
int cantidadDeDatosALeer = 3;

void* Escribir()
{
    for(int i = 0 ; i < ITERACIONES ; i++)
    {
        sem_wait(&sem_escribir);
        pthread_mutex_lock(&mutex);
        while(datos < cantidadDeDatosALeer)//INCREMENTA DATOS HASTA EL LIMITE
        {
        printf("Escribiendo dato...\n");
        datos++;
        }
        pthread_mutex_unlock(&mutex);
        sem_post(&sem_leer);
    }
    pthread_exit(0);
}

void* Leer()
{
    for(int i = 0 ; i < ITERACIONES ; i++)
    {
        sem_wait(&sem_leer);
        pthread_mutex_lock(&mutex);
        while(datos > 0)//DECREMENTA LOS DATOS HASTA 0
        {
            printf("Leyendo dato...\n");
            datos--;
        }
        pthread_mutex_unlock(&mutex);
        sem_post(&sem_escribir);
    }
    pthread_exit(0);
}

int main()
{
    pthread_mutex_init(&mutex,NULL);
    sem_init(&sem_escribir,0,1);
    sem_init(&sem_leer,0,0);

    pthread_t tEscribir, tLeer;
    pthread_create(&tEscribir,NULL,&Escribir,NULL);
	pthread_create(&tLeer,NULL,&Leer,NULL);

    pthread_join(tEscribir,NULL);
    pthread_join(tLeer,NULL);

    pthread_mutex_destroy(&mutex);
    sem_destroy(&sem_escribir);
    sem_destroy(&sem_leer);

    pthread_exit(0);
    return 0;
}